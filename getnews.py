import requests
from bs4 import BeautifulSoup

url = 'http://tw.appledaily.com/hot/daily'
res = requests.get(url)
soup = BeautifulSoup(res.text, 'html.parser')
for i in soup.find(class_ = 'all').find_all('li'):
    num = i.find(class_ = 'aht_title_num').text
    title = i.find(class_ = 'aht_title').text
    view = i.find(class_ = 'aht_pv_num').text
    print(num, title, view)